import React, { useEffect, useState, useContext } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import Toast from 'react-native-simple-toast';
import { validateAll } from 'indicative/validator';
import USERAPIKit, { setUserClientToken, setShopClientToken } from '../../utils/apikit';
import { colors } from '../../res/style/colors'
import Spinner from 'react-native-loading-spinner-overlay';
import Logo from "../../res/assets/images/logo.png"
import {
    Input,
    Card,
    FormValidationMessage,
    Button
} from 'react-native-elements';

import { AuthContext } from '../../utils/authContext';

const LoginScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [mobile, setMobileNumber] = useState('');
    const [password, setPassword] = useState('');
    const [SignUpErrors, setSignUpErrors] = useState({});

    const { signIn, signUp } = useContext(AuthContext);

    const handleSignIn = () => {
        // https://indicative.adonisjs.com
        const rules = {
            mobile: 'required|min:8',
            password: 'required|string|min:6|max:40'
        };

        const data = {
            mobile: mobile,
            password: password
        };

        const messages = {
            required: field => `${field} is required`,
            'username.alpha': 'Username contains unallowed characters',
            'mobile.min': 'Please enter a valid phone number',
            'password.min': 'Wrong Password?'
        };

        validateAll(data, rules, messages)
            .then(() => {
                console.log('success sign in');
                const payload = {mobile, password};
                const onSuccess = ({ data }) => {
                    setLoading(false); 
                    setUserClientToken(data.token);
                    setShopClientToken(data.token);
                    signIn({ mobile, password, token: data.token });
                }
                const onFailure = error => {
                    setLoading(false);
                    console.log(error && error.response);
                    Toast.show('Invalid email or password');
                }
                setLoading(true);
                USERAPIKit.post('/user/login', payload)
                    .then(onSuccess)
                    .catch(onFailure);                
            })
            .catch(err => {
                const formatError = {};
                err.forEach(err => {
                    formatError[err.field] = err.message;
                });
                setSignUpErrors(formatError);
            });
    };

    return (
        <View style={styles.container}>
            <Spinner
                visible={loading} size="large" style={styles.spinnerStyle} />
            <Image style={styles.logoContainer} source={Logo} />
            <View style={styles.inputView}>
                <Input
                    label={'Mobile'}
                    placeholder="Mobile"
                    value={mobile}
                    keyboardType="phone-pad"
                    onChangeText={setMobileNumber}
                    errorStyle={{ color: 'red' }}
                    errorMessage={SignUpErrors ? SignUpErrors.mobile : null}
                />
                <Input
                    label={'Password'}
                    placeholder="Password"
                    value={password}
                    onChangeText={setPassword}
                    secureTextEntry
                    errorStyle={{ color: 'red' }}
                    errorMessage={SignUpErrors ? SignUpErrors.password : null}
                />
                <Button
                    buttonStyle={styles.loginButton}
                    title="Login"
                    onPress={() => handleSignIn()}
                />
                <Button
                    buttonStyle={styles.registerButton}
                    title="Register"
                    onPress={() => signUp()}
                />
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    spinnerStyle: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
    },

    logoContainer: {
        width: 200,
        height: 200,
    },
    inputView: {
        width: "90%",
        borderRadius: 25,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    loginButton: {
        margin: 10,
        marginTop: 30,
    },
    registerButton: {
        margin: 10,
        marginTop: 10,
    }

})
export default LoginScreen;