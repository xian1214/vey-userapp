import React, { useEffect, useState, useContext } from 'react';
import { View, Text, TextInput, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import Toast from 'react-native-simple-toast';
import Autocomplete from 'react-native-autocomplete-input';
import USERAPIKit, { setUserClientToken, setShopClientToken } from '../../utils/apikit';
import Spinner from 'react-native-loading-spinner-overlay';
import {
    Button
} from 'react-native-elements';

import { AuthContext } from '../../utils/authContext';

const RegisterScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobileNumber] = useState('');
    const [password, setPassword] = useState('');
    const [address, setAddress] = useState('');
    const [landmark, setLandmark] = useState('');
    const [city, setCity] = useState('');
    const [query, setQuery] = useState('');
    const [cityData, setCityData] = useState([]);
    const [state, setState] = useState('');
    const [pincode, setPinCode] = useState('');

    const [errorData, setErrorData] = useState(
        {
            isValidName: true,
            isValidEmail: true,
            isValidMobile: true,
            isValidPassword: true,
            isValidAddress: true,            
            email: '',
            mobile: '',
            password: '',
            name: '',
            address: '',
        });

    const { signIn, signOut } = useContext(AuthContext); // should be signUp

    const handleSignUp = () => {
        // https://indicative.adonisjs.com
        if(onNameChanged() == false)
            return;
        if(checkMobileNumber() == false)
            return;
        if(onPasswordChanged() == false)
            return;
        if(onAddressChanged() == false)
            return;
        console.log('success sign in');
        const payload = {mobile, password, email, name, address, landmark, city, state, pincode};
        const onSuccess = ({ data }) => {
            setLoading(false);
            if(data.hasError == false){                
                Toast.show('Successfully registered');
                signOut();
            }
            else
                Toast.show('Failed to register');
        }
        const onFailure = error => {
            setLoading(false);
            console.log(error && error.response);
            Toast.show('Failed to register');
        }
        setLoading(true);
        USERAPIKit.post('/user/signup', payload)
            .then(onSuccess)
            .catch(onFailure);        
    };

    useEffect(() => { }, [errorData]);
    const validateEmail = email => {
        var re = /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    
    const onNameChanged = () => {
        if (name.length < 3) {
            setErrorData({
                ...errorData,
                isValidName: false,
                name: 'Enter the valid name',
            });
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidName: true
            });
            return true;
        }
    }
    const onMobileChanged = () => {
        if (mobile.length >= 8) {
            setErrorData({
                ...errorData,
                isValidMobile: true
            });
            const onSuccessMobile = ({ data }) => {
                setErrorData({
                    ...errorData,
                    isValidMobile: true,
                });
                setLoading(false);
            }
            const onFailureMobile = error => {
                setErrorData({
                    ...errorData,
                    isValidMobile: false,
                    mobile: 'Already existing mobile',
                });
                setLoading(false);
            }
            setLoading(true);
            USERAPIKit.get('/validation/mobile/' + mobile)
                .then(onSuccessMobile)
                .catch(onFailureMobile);
        }
        else {
            setErrorData({
                ...errorData,
                isValidMobile: false,
                mobile: 'Enter the valid phone number',
            });
        }
    }    
    const checkMobileNumber = () => {
        if (mobile.length >= 8) {
            setErrorData({
                ...errorData,
                isValidMobile: true
            });
            return true;
        }
        else {
            setErrorData({
                ...errorData,
                isValidMobile: false,
                mobile: 'Enter the valid phone number',
            });
            return false;
        }
    }
    const checkEmail = () => {
        if (validateEmail(email)) {
            setErrorData({
                ...errorData,
                isValidEmail: true
            });
            return true;
        }
        else {
            setErrorData({
                ...errorData,
                isValidEmail: false,
                email: 'Enter the valid email',
            });
            return false;
        }
    }
    const onEmailChanged = () => {
        if (validateEmail(email)) {
            setErrorData({
                ...errorData,
                isValidEmail: true
            });
            const onSuccessEmail = ({ data }) => {
                setErrorData({
                    ...errorData,
                    isValidEmail: false,
                    email: 'Already existing email',
                });
                setLoading(false);
            }
            const onFailureEmail = error => {
                setErrorData({
                    ...errorData,
                    isValidEmail: true,
                });
                setLoading(false);
            }
            setLoading(true);
            USERAPIKit.get('/validation/email/' + email)
                .then(onSuccessEmail)
                .catch(onFailureEmail);
        }
        else {
            if(email.length > 0){
                setErrorData({
                    ...errorData,
                    isValidEmail: false,
                    email: 'Enter the valid email',
                });    
            }
            else{
                setErrorData({
                    ...errorData,
                    isValidEmail: true,
                });
            }
        }
    }
    const onPasswordChanged = () => {
        if (password.length < 5) {
            setErrorData({
                ...errorData,
                isValidPassword: false,
                password: 'Password length should be greater than 5',
            });
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidPassword: true
            });
            return true;
        }
    }
    const onAddressChanged = () => {
        if (address.length < 3) {
            setErrorData({
                ...errorData,
                isValidAddress: false,
                address: 'Enter the valid address',
            });
            console.log(errorData);
            return false;
        }
        else {
            setErrorData({
                ...errorData,
                isValidAddress: true
            });
            return true;
        }
    }
    const onChangeCity = (city) => {
        console.log(city);
        setCity(city);
        const onSuccessEmail = ({ data }) => {
            setCityData(data.usercities);
        }
        const onFailureEmail = error => {
            setCityData([]);
        }
        USERAPIKit.get('/validation/city/' + city)
            .then(onSuccessEmail)
            .catch(onFailureEmail);
    }
    const onSelectedCity = (item) => {
        setCity(item.city);
        setQuery(item.city);
        setState(item.state);
        setPinCode(item.pincode.toString());
        setCityData([]);
        
    }
    return (
        <View style={styles.container}>
            <Spinner
                visible={loading} size="large" style={styles.spinnerStyle} />
            <ScrollView
                style={styles.scrollView}>
                <View style={styles.inputView}>
                    <View
                        style={{ flexDirection: 'column', marginTop: 10 }}>
                        <Text>
                            Name
                        </Text>
                        <TextInput
                            style={{ borderWidth: 1, borderColor: 'gray', paddingLeft: 8 }}
                            label={'Name'}
                            placeholder="Name"
                            value={name}
                            onChangeText={setName}
                            onBlur={() => onNameChanged()}
                        />
                        {
                            errorData.isValidName ? null : <Text style={{ color: 'red' }}>{errorData.name}</Text>
                        }
                    </View>
                    <View
                        style={{ flexDirection: 'column', marginTop: 10 }}>
                        <Text>
                            Email
                        </Text>
                        <TextInput
                            style={{ borderWidth: 1, borderColor: 'gray', paddingLeft: 8 }}
                            label={'Email'}
                            placeholder="Email Address"
                            value={email}
                            onChangeText={setEmail}
                            onBlur={() => onEmailChanged()}
                        />
                        {
                            errorData.isValidEmail ? null : <Text style={{ color: 'red' }}>{errorData.email}</Text>
                        }
                    </View>
                    <View
                        style={{ flexDirection: 'column', marginTop: 10 }}>
                        <Text>
                            Mobile
                        </Text>
                        <TextInput
                            style={{ borderWidth: 1, borderColor: 'gray', paddingLeft: 8 }}
                            label={'Mobile'}
                            placeholder="Mobile Number"
                            value={mobile}
                            onChangeText={setMobileNumber}
                            onBlur={() => onMobileChanged()}
                        />
                        {
                            errorData.isValidMobile ? null : <Text style={{ color: 'red' }}>{errorData.mobile}</Text>
                        }
                    </View>
                    <View
                        style={{ flexDirection: 'column', marginTop: 10 }}>
                        <Text>
                            Password
                        </Text>
                        <TextInput
                            style={{ borderWidth: 1, borderColor: 'gray', paddingLeft: 8 }}
                            label={'Password'}
                            placeholder="Password"
                            value={password}
                            onChangeText={setPassword}
                            secureTextEntry
                            onBlur={() => onPasswordChanged()}
                        />
                        {
                            errorData.isValidPassword ? null : <Text style={{ color: 'red' }}>{errorData.password}</Text>
                        }
                    </View>
                    <View
                        style={{ flexDirection: 'column', marginTop: 10 }}>
                        <Text>
                            Address
                        </Text>
                        <TextInput
                            style={{ borderWidth: 1, borderColor: 'gray', paddingLeft: 8 }}
                            label={'Landmark'}
                            placeholder="Address"
                            value={address}
                            onChangeText={setAddress}
                            onBlur={() => onAddressChanged()}
                        />
                        {
                            errorData.isValidAddress ? null : <Text style={{ color: 'red' }}>{errorData.address}</Text>
                        }
                    </View>
                    <View
                        style={{ flexDirection: 'column', marginTop: 10 }}>
                        <Text>
                            Landmark
                        </Text>
                        <TextInput
                            style={{ borderWidth: 1, borderColor: 'gray', paddingLeft: 8 }}
                            label={'Landmark'}
                            placeholder="Landmark"
                            value={landmark}
                            onChangeText={setLandmark}
                        />
                    </View>
                    <View
                        style={{ flexDirection: 'column', marginTop: 10 }}>
                        <Text>
                            City
                        </Text>
                        <Autocomplete
                            style={{ borderWidth: 1, borderColor: 'gray', paddingLeft: 8 }}
                            label={'City'}
                            placeholder="City"
                            data={cityData}
                            defaultValue={query}
                            value={city}
                            onChangeText={text=> onChangeCity(text)}
                            renderItem={({ item, i }) => (
                                <TouchableOpacity onPress={() => onSelectedCity(item)}>
                                  <Text>{item.city}</Text>
                                </TouchableOpacity>
                              )}
                        />
                    </View>
                    <View
                        style={{ flexDirection: 'column', marginTop: 10 }}>
                        <Text>
                            State
                        </Text>
                        <TextInput
                            style={{ borderWidth: 1, borderColor: 'gray', paddingLeft: 8 }}
                            label={'State'}
                            placeholder="State"
                            value={state}
                            onChangeText={setState}
                        />
                    </View>
                    <View
                        style={{ flexDirection: 'column', marginTop: 10 }}>
                        <Text>
                            Pincode
                        </Text>
                        <TextInput
                            style={{ borderWidth: 1, borderColor: 'gray', paddingLeft: 8 }}
                            label={'Pincode'}
                            placeholder="Pincode"
                            value={pincode}
                            onChangeText={setPinCode}
                        />
                    </View>

                    <Button
                        buttonStyle={styles.registerButton}
                        backgroundColor="#03A9F4"
                        title="Register"
                        onPress={() => handleSignUp()}
                    />
                    <Text style={{ marginLeft: 80 }} onPress={() => signIn()}>
                        Already Registered? Login
                </Text>
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
    },
    spinnerStyle: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scrollView: {
        marginTop: 50,
    },
    inputView: {
        borderRadius: 25,
        marginBottom: 20,
        justifyContent: "center",
        paddingLeft: 30,
        paddingRight: 30,
    },
    loginButton: {
        margin: 10,
        marginTop: 30,
    },
    registerButton: {
        margin: 10,
        marginTop: 30,
    }

})
export default RegisterScreen;