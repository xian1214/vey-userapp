/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState, useContext } from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  FlatList,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage'
import { SHOPAPIKit, setShopClientToken } from '../../utils/apikit';
import { colors } from '../../res/style/colors'
const OrderHistoryScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [orders, setOrders] = useState([]);
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      const bootstrapAsync = async () => {
        let userToken = null;
        try {
          userToken = await AsyncStorage.getItem('userToken')
        } catch (e) {
          console.log(e);
        }
        if (userToken != null) {
          const onSuccess = ({ data }) => {
            setLoading(false);
            getOrders(data.orders);
            console.log(data);
          }
          const onFailure = error => {
            console.log(error);
            setLoading(false);
          }
          setLoading(true);
          SHOPAPIKit.get('/userorder/orders')
            .then(onSuccess)
            .catch(onFailure);
        }

      };
      bootstrapAsync();
    });
    return unsubscribe;

  }, [navigation]);
  const getOrders = (orders) => {
    setOrders(orders);
  }
  const onOrderPressed = (item) => {
    console.log(item.orderref);
    navigation.navigate('Order Detail', item)
  }
  const renderItem = ({ item }) => {
    return (
      <View style={styles.item}>
        <TouchableOpacity onPress={() => onOrderPressed(item)}>
          <View style={{ flexDirection: 'column', padding: 8, justifyContent: 'space-between' }}>
            <Text style={{ fontSize: 15 }}>Ref {item.orderref}</Text>
            <Text style={{ fontSize: 15 }}>Quantity {item.orderquantity}</Text>
            <Text style={{ fontSize: 15 }}>Sub Total ₹{item.ordertotal}</Text>
            <Text style={{ fontSize: 15 }}>Date {item.orderdate}</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
  return (
    <>
      <View style={styles.container}>
        <Spinner
          visible={loading} size="large" style={styles.spinnerStyle} />
        <FlatList
          data={orders}
          keyExtractor={(item, index) => index.toString()}
          renderItem={orders ? renderItem : null} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listContainer: {
    flex: 1,
  },
  item: {
    flex: 1,
    margin: 10,
    flexDirection: 'row',
    backgroundColor: colors.white,
  },
});

export default OrderHistoryScreen;