/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
  SafeAreaView,
  StyleSheet,
  FlatList,
  Image,
  View,
  Button,
  Text,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage'
import Toast from 'react-native-simple-toast';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';
import { colors } from '../../res/style/colors'
import USERAPIKit, { setUserClientToken } from '../../utils/apikit';
import Logo from "../../res/assets/images/logo.png"


const CartScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [total, setTotal] = useState(0);
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      // Call any action
      const bootstrapAsync = async () => {
        let userToken = null;
        try {
          userToken = await AsyncStorage.getItem('userToken')
        } catch (e) {
          console.log(e);
        }
        if (userToken != null) {
          const onSuccess = ({ data }) => {
            setLoading(false);
            //console.log(data);
            getTotalPrice(data.products);
          }
          const onFailure = error => {
            setLoading(false);
            setData([]);
            //console.log(error);
          }
          setLoading(true);
          USERAPIKit.get('/user/cart/detail')
            .then(onSuccess)
            .catch(onFailure);
        }
        else {
          setData([]);
        }
      };
      bootstrapAsync();
      console.log('Focused effect')
  
    });
    return unsubscribe;
  }, [navigation]);
  const getTotalPrice = (data) => {
    var tempTotal = 0;
    data.forEach(element => {
      if (element.quantity != 'undefined' && element.quantity > 0) {
        var itemTotal = element.unitprice * element.quantity;
        element.total = itemTotal;
        tempTotal += itemTotal;
      }
    });
    setData(data);
    setTotal(tempTotal);
  }
  const orderProduct = () => {
    navigation.navigate('Place Order');
  }
  const renderCircleView = (item) => {
    if (item.symbol == 'G') {
      return (
        <View style={styles.circleview_green} />
      )
    }
    else if (item.symbol == 'R') {
      return (
        <View style={styles.circleview_red} />
      )
    }
    else if (item.symbol == 'Y') {
      return (
        <View style={styles.circleview_yellow} />
      )
    }
    else if (item.symbol == 'B') {
      return (
        <View style={styles.circleview_brown} />
      )
    }
    else {
      return (
        <View style={styles.circleview_white} />
      )
    }

  }
  const renderItem = ({ item }) => {
    return (
      <View style={styles.item}>
        <Image
          style={styles.image}
          source={item.imageurl ? { uri: item.imageurl } : Logo}
        />
        <View style={{ flex: 1 }}>
          <Text style={{ marginTop: 10, fontSize: 16 }}>{item.product}</Text>
          <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <Text style={{ flex: 1, fontSize: 15 }}>{item.brand}</Text>
            <Text style={{ flex: 1, fontSize: 15, marginLeft: 10 }}>{item.weight} {item.weightunit}</Text>
            {
              renderCircleView(item)
            }
          </View>
          <View style={{ flexDirection: 'row', marginTop: 4, justifyContent: 'space-between' }}>
            <Text style={{ fontSize: 15 }}>₹ {item.unitprice}</Text>
            <View style={{ height: 20, marginRight: 10, marginBottom: 5, }}>
              <Text style={{ fontSize: 15 }}>{item.quantity}</Text>
            </View>
            <Text style={{ fontSize: 15 }}>₹ {item.total}</Text>
          </View>
        </View>
      </View>
    )
  }

  return (
    <>
      <View style={styles.container}>
        <Spinner
          visible={loading} size="large" style={styles.spinnerStyle} />
        <Text style={{ marginTop: 10, fontSize: 16, alignSelf: "center" }}>Total Price: {total}</Text>
        <FlatList
          data={data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={data ? renderItem : null} />
        {
          data.length != 0 ?
            <View style={styles.buttonContainer}>
              <Button title='Place Order' onPress={orderProduct} />
            </View> : null
        }

      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listContainer: {
    flex: 1,
  },
  item: {
    flex: 1,
    margin: 10,
    flexDirection: 'row',
    backgroundColor: colors.white,
  },
  image: {
    width: 100,
    height: 100,
    resizeMode: 'stretch'
  },
  circleview_green: {
    marginRight: 10,
    width: 14,
    height: 14,
    borderRadius: 14 / 2,
    backgroundColor: '#32CD32'
  },
  circleview_red: {
    marginRight: 10,
    width: 14,
    height: 14,
    borderRadius: 14 / 2,
    backgroundColor: '#8B0000'
  },
  circleview_brown: {
    marginRight: 10,
    width: 14,
    height: 14,
    borderRadius: 14 / 2,
    backgroundColor: '#D2691E'
  },
  circleview_white: {
    marginRight: 10,
    width: 14,
    height: 14,
    borderRadius: 14 / 2,
    backgroundColor: '#FFFFFF'
  },
  circleview_yellow: {
    marginRight: 10,
    width: 14,
    height: 14,
    borderRadius: 14 / 2,
    backgroundColor: '#808000'
  },
  buttonContainer: {
    height: 35,
    justifyContent: 'center',
    marginLeft: 25,
    marginRight: 25,
    marginTop: 10,
    marginBottom: 10,
  }
});

export default CartScreen;