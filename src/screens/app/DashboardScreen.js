/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  Text,
} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { DrawContent } from './DrawerContent';
import HomeScreen from './HomeScreen';
import ProfileScreen from './ProfileScreen';
import LiveOrderScreen from './LiveOrderScreen';
import CartScreen from './CartScreen';
import PlaceOrderScreen from './PlaceOrderScreen';
import OrderSummaryScreen from './OrderSummaryScreen';
import SearchScreen from './SearchScreen';
import OrderHistoryScreen from './OrderHistoryScreen';
import OrderDetailScreen from './OrderDetailScreen';
import SubscriptionScreen from './SubscriptionScreen';
import { colors } from '../../res/style/colors'
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();


const HomeStackScreen = ({ navigation }) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <MaterialCommunityIcons.Button name="menu" size={25}
              backgroundColor={colors.primary}
              onPress={() => navigation.openDrawer()}
            ></MaterialCommunityIcons.Button>
          ),
          headerRight: () => (
            <MaterialCommunityIcons.Button name="magnify" size={25}
              backgroundColor={colors.primary}
              onPress={() => navigation.navigate('Search')}
            ></MaterialCommunityIcons.Button>
          )
        }}
      />
      <Stack.Screen
        name="Search"
        component={SearchScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
    </Stack.Navigator>
  );
};

const LiveOrderStackScreen = ({ navigation }) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="LiveOrder"
        component={LiveOrderScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <MaterialCommunityIcons.Button name="menu" size={25}
              backgroundColor={colors.primary}
              onPress={() => navigation.openDrawer()}
            ></MaterialCommunityIcons.Button>
          )
        }}
      />
      <Stack.Screen
        name="Order Detail"
        component={OrderDetailScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
    </Stack.Navigator>
  );
};

const CartStackScreen = ({ navigation }) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Cart"
        component={CartScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <MaterialCommunityIcons.Button name="menu" size={25}
              backgroundColor={colors.primary}
              onPress={() => navigation.openDrawer()}
            ></MaterialCommunityIcons.Button>
          )
        }}
      />
      <Stack.Screen
        name="Place Order"
        component={PlaceOrderScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="Order Summary"
        component={OrderSummaryScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
    </Stack.Navigator>

  );
};
const OrderHistoryStackScreen = ({ navigation }) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Order History"
        component={OrderHistoryScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <MaterialCommunityIcons.Button name="menu" size={25}
              backgroundColor={colors.primary}
              onPress={() => navigation.openDrawer()}
            ></MaterialCommunityIcons.Button>
          )
        }}
      />
      <Stack.Screen
        name="Order Detail"
        component={OrderDetailScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
    </Stack.Navigator>
  );
};
const SubscriptionStackScreen = ({ navigation }) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Subscription"
        component={SubscriptionScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <MaterialCommunityIcons.Button name="menu" size={25}
              backgroundColor={colors.primary}
              onPress={() => navigation.openDrawer()}
            ></MaterialCommunityIcons.Button>
          )
        }}
      />
    </Stack.Navigator>
  );
};
const ProfileStackScreen = ({ navigation }) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <MaterialCommunityIcons.Button name="menu" size={25}
              backgroundColor={colors.primary}
              onPress={() => navigation.openDrawer()}
            ></MaterialCommunityIcons.Button>
          )
        }}
      />
    </Stack.Navigator>
  );
};

const DashboardScreen = ({ navigation }) => {
  return (
    <>
      <Drawer.Navigator
        initialRouteName="Home"
        drawerContent={props => <DrawContent {...props} />}>
        <Drawer.Screen
          name="Home"
          component={HomeStackScreen} />
        <Drawer.Screen
          name="Profile"
          component={ProfileStackScreen} />
        <Drawer.Screen
          name="Subscription"
          component={SubscriptionStackScreen} />
        <Drawer.Screen
          name="Cart"
          component={CartStackScreen} />
        <Drawer.Screen
          name="Order History"
          component={OrderHistoryStackScreen} />
        <Drawer.Screen
          name="Live Order"
          component={LiveOrderStackScreen} />
      </Drawer.Navigator>
    </>
  );
};


export default DashboardScreen;