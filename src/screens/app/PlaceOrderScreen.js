/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState, useContext } from 'react';
import MapView, { Marker } from 'react-native-maps'
import AwesomeAlert from 'react-native-awesome-alerts'; import {
  SafeAreaView,
  StyleSheet,
  Image,
  ScrollView,
  View,
  Text,
} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-community/async-storage'
import Spinner from 'react-native-loading-spinner-overlay';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { SHOPAPIKit, setShopClientToken } from '../../utils/apikit';
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import SwitchSelector from 'react-native-switch-selector';
const PlaceOrderScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([])
  const [markers, setMarkers] = useState([])
  const [region, setRegion] = useState({
    latitude: 15.480808256,
    longitude: 73.82310486,
    latitudeDelta: 0.01,
    longitudeDelta: 0.01,
  })
  const [options, setOptions] = useState([
    { label: 'Discount', value: '0' },
    { label: 'Distance', value: '1' },
    { label: 'Wholesaler', value: '2' }
]);
  const [shop, setShop] = useState({
    index: 0,
    alert: false,
    wholesaler: 0,
    bulkorder: 0,
    shopname: '',
    year: 1990
  })
  const [marginBottom, setMarginBottom] = useState(0);
  const [payload, setPayload] = useState({ latitude: 15.475840, longitude: 73.819714, area: 0 });
  useEffect(() => {
    setShop({
      alert: false
    });

    const unsubscribe = navigation.addListener('focus', () => {
      const bootstrapAsync = async (coords) => {
        let userToken = null;
        try {
          userToken = await AsyncStorage.getItem('userToken')
        } catch (e) {
          console.log(e);
        }
        if(userToken != null){
          onSwitchChange(0);
        }  
      };
      Geolocation.getCurrentPosition(info => 
        {
          if(info.coords != undefined){
            setRegion({latitude: info.coords.latitude, longitude: info.coords.longitude})                  
            setPayload({latitude:info.coords.latitude, longitude: info.coords.longitude, area: 0})
            bootstrapAsync(info.coords)
          }
        }
      );
    });
    return unsubscribe;

  }, [navigation]);
  const updateShopList = (shopData) => {
    console.log('------------------------');
    console.log(shopData);
    while (markers.length) {
      markers.pop();
    }
    shopData.forEach(element => {
      if (element.shopname != 'undefined') {
        markers.push({ latitude: element.latitude, longitude: element.longitude, title: element.shopname, subtitle: element.shopname })
      }
//      console.log(markers)
    });
  }
  const onPressMarker = (index) => {
//    console.log("marker index=" + index);
//    setShop({ alert: true, index: index, shopname: data[index].shopname, bulkorder: data[index].bulkorder, wholesaler: data[index].wholesaler, year: data[index].Incorporationyear });
    navigation.navigate('Order Summary', {shopname: data[index].shopname, shopcode: data[index].shopcode, discount: data[index].discount});
  }
  const onMapReady = () => {
    console.log("map ready");
    setMarginBottom(1);
  }
  const onRegionChange = (region) => {
    setRegion(region);
  }
  const onSwitchChange = (value) => {
    console.log(value);    
    const onSuccess = ({ data }) => {
      setData(data.shops)
      updateShopList(data.shops);
      setLoading(false);

    }
    const onFailure = error => {
      console.log(error);
      setLoading(false);
    }
    setLoading(true);
    console.log(payload);
    var requestUrl = '/shopoperation/discount';
    if(value == 0){
      requestUrl = '/shopoperation/discount';
    } 
    else if(value == 1){
      requestUrl = '/shopoperation/near';
    }
    else{
      requestUrl = '/shopoperation/wholesaler';
    }
   SHOPAPIKit.post(requestUrl, payload)
   .then(onSuccess)
   .catch(onFailure);

  }
  return (
    <>
      <View style={styles.container}>
        <Spinner
          visible={loading} size="large" style={styles.spinnerStyle} />
        <MapView
          style={{ flex: 1, marginBottom: marginBottom }}
          onMapReady={onMapReady}
          initialRegion={{
            latitude :region.latitude != null ? region.latitude : 15.480808256,
            longitude: region.longitude != null ? region.longitude : 73.82310486,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
          }}
          region={{
            latitude :region.latitude != null ? region.latitude : 15.480808256,
            longitude: region.longitude != null ? region.longitude : 73.82310486,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
          }}
          showsMyLocationButton={true}
          showsUserLocation={true}>
          {
            markers.map((marker, index) => (
              <Marker
                key={index}
                coordinate={{ latitude: marker.latitude, longitude: marker.longitude }}                
                onPress={() => onPressMarker(index)}>
                <View >
                  <View
                    style={styles.callout}>
                    <Text style={styles.titleText}>
                      {marker.title}
                    </Text>
                  </View>                  
                  <View
                    style={{alignSelf:'center'}}>
                    <MaterialCommunityIcons
                        name="map-marker"
                        color="red"
                        size={40}
                      />
                  </View>
                </View>
                <MapView.Callout
                  tooltip={true}
                  />
              </Marker>
            )
            )}
        </MapView>
        <SwitchSelector options={options} initial={0} onPress={value => onSwitchChange(value)} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#FFFFFF',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  callout: {
    backgroundColor: "white",
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
    padding: 4
  },
  titleText: {
    color: "black",
    fontSize: 14,
    lineHeight: 18,
    flex: 1,
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default PlaceOrderScreen;