/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  FlatList,
  Button,
  View,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import NumericInput from 'react-native-numeric-input'
import AwesomeAlert from 'react-native-awesome-alerts';
import AsyncStorage from '@react-native-community/async-storage'
import { Toolbar, Searchbar } from 'react-native-paper';
import { colors } from '../../res/style/colors'
import USERAPIKit, { SHOPAPIKit, setShopClientToken } from '../../utils/apikit';
import Logo from "../../res/assets/images/logo.png"
const SearchScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [query, setQuery] = useState('');
  const [alert, setAlert] = useState(false);
  /*
  useEffect(() => {
    const bootstrapAsync = async () => {
        let userToken = null;
        try {
            userToken = await AsyncStorage.getItem('userToken')
            console.log(userToken)
        } catch (e) {
            console.log(e);
        }
    };
    bootstrapAsync();

  }, []);
*/
  const handleSearch = (query) => {
    setQuery(query);
    if (query.length >= 3) {
      const onSuccess = ({ data }) => {                
        setData(data.products);
      }
      const onFailure = error => {
        setData([]);

      }
      SHOPAPIKit.get('/product/' + query)
        .then(onSuccess)
        .catch(onFailure);
    }
    else {
      setData([]);
    }
  }

  const confirmProduct = () => {
    var products = { products: []}
    data.forEach(element => {
      if(element.quantity != 'undefined' && element.quantity > 0) 
      {
        products.products.push({"productid": element.productid, "quantity": element.quantity})
      }
    });
    const onSuccess = ({ data }) => {
      setLoading(false);
      Toast.show('Successfully updated');
    }
    const onFailure = error => {
      setLoading(false);
      Toast.show('Failed to update.');
    }
    setLoading(true);
//    console.log(products);
    USERAPIKit.post('/user/cart/update', products)
      .then(onSuccess)
      .catch(onFailure);
    setAlert(false)
  }
  const updateProduct = () => {
    var count = 0;
    data.forEach(element => {
      if(element.quantity != 'undefined' && element.quantity > 0) 
      {
        count++;
      }
    });
    if(count == 0){
        Toast.show('Please select product first', Toast.SHORT);
    }
    else{
      setAlert(true);
    }
  }
  const renderCircleView = (item) => {
    if(item.symbol == 'G'){
      return(
        <View style={styles.circleview_green} />
      )  
    }
    else if(item.symbol == 'R'){
      return(
        <View style={styles.circleview_red} />
      )  
    }
    else if(item.symbol == 'Y'){
      return(
        <View style={styles.circleview_yellow} />
      )  
    }
    else if(item.symbol == 'B'){
      return(
        <View style={styles.circleview_brown} />
      )  
    }
    else{
      return(
        <View style={styles.circleview_white} />
      )
    }

  }
  const renderItem = ({ item }) => {
    return (
      <View style={styles.item}>
        <Image
          style={styles.image}
          source={item.imageurl ? { uri: item.imageurl } : Logo}
        />
        <View style={{ flex: 1 }}>
          <Text style={{ marginTop: 10, fontSize: 16 }}>{item.product}</Text>
          <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <Text style={{ flex:1, fontSize: 15 }}>{item.brand}</Text>
            <Text style={{ flex:1, fontSize: 15, marginLeft: 10 }}>{item.weight} {item.weightunit}</Text>            
            {
              renderCircleView(item)
            }            
          </View>
          <View style={{ flexDirection: 'row', marginTop: 4, justifyContent: 'space-between' }}>
            <Text style={{ marginTop: 5, fontSize: 15 }}>₹ {item.unitprice}</Text>
            <View style={{height:20, marginRight: 10, marginBottom: 5,}}>
              <NumericInput                 
                minValue={0}
                totalHeight={35}
                rounded={true}
                leftButtonBackgroundColor={colors.primary}
                rightButtonBackgroundColor={colors.primary}
                onChange={value=> item.quantity = value}
                />
            </View>
          </View>
        </View>
      </View>
    )
  }
  return (
    <>
      <SafeAreaView style={styles.container}>
        <Searchbar
          placeholder="Product Name"
          onChangeText={(query) => handleSearch(query)}
          value={query}
          style={styles.searchbar}
        />
        <View
          style={styles.listContainer}>
          <FlatList
            data={data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={data ? renderItem : null} />
            {
              data.length != 0 ? 
              <View style={styles.buttonContainer}>
                <Button title='Update' onPress={updateProduct} />
              </View> : null  
            }
        </View>        
        <Spinner
          visible={loading} size="large" style={styles.spinnerStyle} />
        <AwesomeAlert
          show={alert}
          showProgress={false}
          title="Alert"
          message="Are you sure?"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="No"
          confirmText="Yes"
          confirmButtonColor="#DD6B55"
          onCancelPressed={() => {
            setAlert(false);
          }}
          onConfirmPressed={() => {
            confirmProduct();
          }}
        />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listContainer: {
    flex: 1,
  },
  searchbar: {
    margin: 4,
  },
  item: {
    flex: 1,
    margin: 10,
    flexDirection: 'row',
    backgroundColor: colors.white,
  },
  image: {
    width: 100,
    height: 100,
    resizeMode: 'stretch'
  },
  circleview_green: {
    marginRight: 10,
    width: 14,
    height: 14,
    borderRadius: 14 / 2,
    backgroundColor: '#32CD32'
  },
  circleview_red: {
    marginRight: 10,
    width: 14,
    height: 14,
    borderRadius: 14 / 2,
    backgroundColor: '#8B0000'
  },
  circleview_brown: {
    marginRight: 10,
    width: 14,
    height: 14,
    borderRadius: 14 / 2,
    backgroundColor: '#D2691E'
  },
  circleview_white: {
    marginRight: 10,
    width: 14,
    height: 14,
    borderRadius: 14 / 2,
    backgroundColor: '#FFFFFF'
  },
  circleview_yellow: {
    marginRight: 10,
    width: 14,
    height: 14,
    borderRadius: 14 / 2,
    backgroundColor: '#808000'
  },
  buttonContainer: {
    height: 35,
    justifyContent: 'center',
    marginLeft: 25,
    marginRight: 25,
    marginTop: 10,
    marginBottom: 10,
  }
});

export default SearchScreen;